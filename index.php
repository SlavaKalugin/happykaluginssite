<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Happy-kalugins</title>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="js/supersized/supersized.core.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>	<script type="text/javascript" src="js/jquery.localscroll.js"></script>
	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox -->
	<link rel="stylesheet" href="js/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="js/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

	<script type="text/javascript" src="js/jquery.scrollTo.min.js"></script>
	<script type="text/javascript" src="js/jquery.localScroll.js"></script>
		<script type="text/javascript">
		jQuery(function( $ ){
		

			var bodyH = $(window).height() / 2;
			$("body").after(
				'<div id="system-load" style="position:fixed;'
				+ 'top:0px; left:0px; background: #BFEDED; width:100%; height:100%;'
				+ 'z-index:99999999; color:#fff; padding-top:' + bodyH + 'px;"'
				+ 'align="center">'
				+ '<img style="" src="images/logo.png"'
				+ ' alt="Пожалуйста, подождите..." /></div>'
			);
			$(window).bind('load', function() {
				$('#system-load').fadeOut('slow').remove();
				$('body').css({"opacity": "0", "display":"block"});
				$('body').animate({opacity: 1}, 'fast');
			});

		
			$.localScroll({
				queue:true,
				duration:1000,
				hash:true
			});
			
			$('.point_heart').hover(function(){
				$('.point_heart_content').fadeIn();
			},
			function(){
					$('.point_heart_content').fadeOut();
				}
			);
			
			$(".popup").fancybox({
				maxWidth	: 800,
				maxHeight	: 600,
				fitToView	: false,
				width		: '70%',
				height		: '70%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
			
			
			$('.points').hover(
				function(){
					$(this).children('.click').fadeIn();
				},
				function(){
					$(this).children('.click').hide();
				}
			);
			
			$('.points').on('click',function(){
				$('.activeBoll').removeClass('activeBoll');
				$('.hide').hide();
				$(this).addClass('activeBoll');
				var activClass = $(this).attr('link');
				$('.'+activClass).fadeIn();
			});
			
		});
	</script>
</head>

<body>
	<div class="wrapp">
	
		<div class="photo_one"><img src="images/photo_one.png" alt="Happy Kalugins" title="Happy Kalugins" /></div>
		<div class="photo_two"><img src="images/photo_two.png" alt="Happy Kalugins" title="Happy Kalugins" /></div>
		<div class="logo"><img src="images/logo.png" alt="Happy Kalugins" title="Happy Kalugins" /></div>
		<div class="wrapp_menu">
			<ul class="menu">
				<li><a href="#history" >Наша история</a></li>
				<li><a href="#history" >Галерея</a></li>
				<li class="item_three"><a href="#wedding_day" >ЗАГС</a></li>
				<li><a href="#wedding_day" >Ресторан</a></li>
			</ul>
			<div class="clr"></div>
		</div>

		<div  class="banner_right">
			<a class="video_link popup fancybox.iframe" href="http://www.youtube.com/embed/9-Cj9Mb1lcM?rel=0&autoplay=1">Вспомнить как это было</a>
		</div>
		<div class="height_block"></div>
		<div class="content" id="history">
		
			<div class="title_about"><img src="images/title_about.png" title="About Kalugins" alt="About Kalugins"></div>
		
			<div class="she">
				<div class="left_col">
					<img src="images/about_she.png" title="Lena Kalugina" alt="Lena Kalugina" />
				</div>
				<div class="right_col text">
					<h2>О ней</h2>
					<p>
						Эта девушка родилась в морозный день 4 декабря 1987 года. В семье она была «младшенькой кудряшкой» и как все младшие дети всегда 
						подрожала своей старшей сестре, мудренной опытом четырех с половиной лет жизни до рождения младшей сестры.
						<br/><br/>
					</p>
				</div>
			</div>
			<div class="he">
				<div class="left_col text">
					<h2>О нём</h2>
					<p>
						Этот молодец родился 19 ноября 1987 года в столице нашей прекрасной страны в родильном доме номер 67 :|> . 
						<br/><br/>
				</div>
				<div class="right_col">
					<img src="images/about_he.png" title="Slava Kalugin" alt="Slava Kalugin" />
				</div>
			</div>
			<div class="clr"></div>
			
			<div class="history">
				
				<div class="points h_point_item_one" link="h_point_item_one_content"><img src="images/h_one.png" class="img_small"/><div class="click">Нажми</div></div>
				<div class="h_point_item_one_content text hide">
					<p>
						Детство проходило вполне обычно: бегал по гаражам, гонял голубей, играл в ножички с друзьями и ломал руки.
					</p>
					<p>
						<a class="popup" rel="gallery1" href="images/hg_one.jpg" /><img src="images/hgs_one.jpg" alt='Kind' /></a>
						<a class="popup" rel="gallery1"  href="images/hg_two.jpg" /><img src="images/hgs_two.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/hg_three.jpg" /><img src="images/hgs_three.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/hg_four.jpg" /><img src="images/hgs_four.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/hg_five.jpg" /><img src="images/hgs_five.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/hg_six.jpg" /><img src="images/hgs_six.jpg"/></a>
					</p>
				</div>
				
				
				<div class="points h_point_item_two" link="h_point_item_two_content"><img src="images/h_two.png" class="img_small"/><div class="click">Нажми</div></div>
				<div class="h_point_item_two_content text hide">
					<p>
						К середине школьных годов он начал увлекаться девчонками, тяжёлой музыкой, в какой-то момент ходил с длинными волосами, но когда надо было брался за ум, учился, посещал занятия спортом ( футболом, воллейболом, баскетболом ), играл с друзьями в хоккей, пытался научиться играть на бас-гитаре).
					</p>
					<p>
						<a class="popup" rel="gallery1" href="images/d1.jpg" /><img src="images/ds1.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/d2.jpg" /><img src="images/ds2.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/d3.jpg" /><img src="images/ds3.jpg"/></a>
						<a class="popup" rel="gallery1" href="images/d4.jpg" /><img src="images/ds4.jpg"/></a>
					</p>
				</div>
				
				<div class="points h_point_item_three" link="h_point_item_three_content"><img src="images/h_three.png" class="img_small"/><div class="click">Нажми</div></div>
				<div class="h_point_item_three_content text hide">
					<p>
						И вот уже настало университетское время РУДН, наш герой уже считал себя взрослым, родители перехали жить отдельно) На первых курсах университета, естстественно как и все правильные парни он не учился, занимался спортом, устраивал вечеринки. Ближе к концу учёбы собрался с силами и решил, что надо выбирать профессию и получать знания. И надо отметить, что у него есть диплом бакалавриата и степень магистра). 
					</p>
					<p>
						<a class="popup" rel="gallery1"  href="images/d5.jpg" /><img src="images/ds5.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/d6.jpg" /><img src="images/ds6.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/d12.jpg" /><img src="images/ds12.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/d11.jpg" /><img src="images/ds11.jpg"/></a>
						<a class="popup" rel="gallery1" href="images/d10.jpg" /><img src="images/ds10.jpg"/></a>
					</p>
				</div>
				
				<div class="points h_point_item_four" link="h_point_item_four_content"><img src="images/h_four.png" class="img_small"/><div class="click">Нажми</div></div>
				<div class="h_point_item_four_content text hide">
					<p>
						Университет закончился, спорт и гулянки с друзьями продолжались и вот как-то на дне рождении бывшей одногрупницы-будующей жены, он как-то увидел её в другом свете и всё пошло поехало. 
					</p>
					<p>
						<a class="popup" rel="gallery1" href="images/d7.jpg" /><img src="images/ds7.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/d8.jpg" /><img src="images/ds8.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/d9.jpg" /><img src="images/ds9.jpg"/></a>
					</p>
				</div>
				
				
				<div class="points h_point_item_five" link="h_point_item_five_content"><img src="images/h_seven.png" class="img_small"/><div class="click">Нажми</div></div>
				<div class="h_point_item_five_content text hide">
					<p>
						После проведения множества совместно приятных мгновений, он понял, что это та единственная и решился сделать предложение. А дальше вы всё знаете) Отдельное спасибо всем друзьям, которые его поддерживали и наставляли на правильный путь и сестре за помощь в выборе того самого кольца) 
					</p>
					<p>
						<a class="popup" rel="gallery1" href="images/d13.jpg" /><img src="images/ds13.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/d14.jpg" /><img src="images/ds14.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/d15.jpg" /><img src="images/ds15.jpg"/></a>
						<a class="popup" rel="gallery1"  href="images/d16.jpg" /><img src="images/ds16.jpg"/></a>
					</p>
				</div>
				
				
				<div class="points s_point_item_one" link="s_point_item_one_content"><img src="images/s_one.png" class="img_small"/><div class="click">Нажми</div></div>
				<div class="s_point_item_one_content text hide">
					<p>
						Младшая сестра всегда просилась
						с ней на прогулки и в кино, иногда они ссорились и, что скрывать, даже 
						дрались... Ну а когда младшая понимала, что старшая сестра сильнее, то 
						всегда бежала за защитой к маме с папой, которые часто принимали позицию 
						младшей стороны.
					</p>
					<p>
						<a class="popup" rel="gallery2" href="images/sg_one.jpg" /><img src="images/sgs_one.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/sg_two.jpg" /><img src="images/sgs_two.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/sg_three.jpg" /><img src="images/sgs_three.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/sg_four.jpg" /><img src="images/sgs_four.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/sg_five.jpg" /><img src="images/sgs_five.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/sg_six.jpg" /><img src="images/sgs_six.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r14.jpg" /><img src="images/rs14.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r15.jpg" /><img src="images/rs15.jpg"/></a>
					</p>
				</div>
				
				
				<div class="points s_point_item_two" link="s_point_item_two_content"><img src="images/s_two.png" class="img_small"/><div class="click">Нажми</div></div>
				<div class="s_point_item_two_content text hide">
					<p>
						В детстве, благодаря маминому упорству, она посещала много кружков и
						даже секций (например, настольного тенниса). Какие то кружки удалось 
						закончить и даже «с похвалой». Выпускная работа в художественной 
						школе по батику украшает один из залов Центрального дома скульптора. И 
						конечно танцы, с которыми в жизни девушки связано огромное количество 
						счастливых моментов.
					</p>
					<p>
						<a class="popup" rel="gallery2" href="images/r1.jpg" /><img src="images/rs1.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r2.jpg" /><img src="images/rs2.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r3.jpg" /><img src="images/rs3.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r16.jpg" /><img src="images/rs16.jpg"/></a>
					</p>
				</div>
				
				<div class="points s_point_item_three" link="s_point_item_three_content"><img src="images/s_three.png" class="img_small"/><div class="click">Нажми</div></div>
				<div class="s_point_item_three_content text hide">
					<p>
						После окончания школы, девушка была в растерянности: куда идти учиться? 
						Главным в определении будущей профессии было огромное желание –
						никогда больше не писать сочинений. Первое место заняла профессия 
						инженера: инженеры совсем не пишут сочинений!
					</p>
					<p>
						Она поступила на инженерный факультет Российского университета дружбы 
						народов. Через пару лет обучения в РУДН, она поняла, что сделала неплохой 
						выбор. Обучаться в группе среди умных компьютерных гениев, а еще и 
						красивых ребят, было, безусловно, приятно и обижаться на недостаток
						мужского внимания не приходилось.
					</p>
					<p>
						<a class="popup" rel="gallery2" href="images/r4.jpg" /><img src="images/rs4.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r5.jpg" /><img src="images/rs5.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r6.jpg" /><img src="images/rs6.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r17.jpg" /><img src="images/rs17.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r18.jpg" /><img src="images/rs18.jpg"/></a>
					</p>
				</div>
				
				<div class="points s_point_item_four" link="s_point_item_four_content"><img src="images/s_four.png" class="img_small"/><div class="click">Нажми</div></div>
				<div class="s_point_item_four_content text hide">
					<p>
						На протяжении всех семи лет обучения ее одногруппники оставались для 
						нее только одногруппниками и хорошими друзьями. 
						Но как показала жизнь, что, если присмотреться к своим друзьям 
						повнимательнее, то среди них может оказаться и человек, с которым ты не 
						захочешь расставаться, тот, кто тебе, оказывается, очень дорог... 
					</p>
					<p>
						После окончания университета, они встретились на ее дне рождении. И она 
						поняла, что совсем его не знала. За днем рождения последовала встреча, за 
						первой встречей вторая, прогулки по зимней набережной. 
					</p>
					<p>
						<a class="popup" rel="gallery2" href="images/r7.jpg" /><img src="images/rs7.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r8.jpg" /><img src="images/rs8.jpg"/></a>
					</p>
					
				</div>
				
				
				<div class="points s_point_item_five" link="s_point_item_five_content"><img src="images/s_seven.png" class="img_small"/><div class="click">Нажми</div></div>
				<div class="s_point_item_five_content text hide">
					<p>
						И незаметно она

						поняла, что постоянно думает о нем, говорит о нем, вспоминает о нем... 

						Она теперь даже полюбила зиму, потому что зима напоминает ей о первых 

						трепетных минутах взаимного чувства, первых поцелуях и признаниях...
					</p>
					<p>
						И вот через год уже совместная поездка на машине в Хельсинки. Долгая и 

						веселая дорога:только он и она в машинке, но настолько уютно и тепло, что 

						В Хельсинке, зимой она и сказала, то самое «ДА!» в ответ на предложение 

						того единственного, с кем она твердо решила идти дальше по жизни рука в 

						руке, запрыгнув к нему в лодку, они решиил плыть вместе!
					</p>
					<p>
						<a class="popup" rel="gallery2" href="images/r9.jpg" /><img src="images/rs9.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r10.jpg" /><img src="images/rs10.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r11.jpg" /><img src="images/rs11.jpg"/></a>
						<a class="popup" rel="gallery2"  href="images/r12.jpg" /><img src="images/rs12.jpg"/></a>
					</p>
					<p>
						 Она безумно счастлива и ждет с нетерпением одного из самых главных событий в жизни.<br/> 

						Как здорово, что все происходит так, как происходит!!! 
					</p>
				</div>
				
				
				
				
				<div class="point_heart">25<br/><span class="moun">ДЕК</span></div>
				<div class="point_heart_content text">
					
				</div>
				<a class="point_wedding popup fancybox.iframe" href="http://www.youtube.com/embed/9-Cj9Mb1lcM?rel=0&autoplay=1"></a>
			</div>
			<div class="wedding_day"  id="wedding_day">
				<div class="left_col text_light">
					<img src="images/zags.png" title="ЗАГС Калугины" alt="ЗАГС Калугины" />
					<h3>ЗАГС - 10:20 - 11:30</h3>
					<div class="clr"></div>
					<p>
						Торжественная церемония состоится во Дворце бракосочетания №4 :)<br/>
						<span>Адрес:</span> Москва, метро Савёловская, выход из метро один, далее пешком 7-10 минут до ул. Бутырская, д.17<br/>
						<a class="popup fancybox.iframe" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2242.7095618345356!2d37.58326300000001!3d55.79828100000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b549fb7350850d%3A0xca1f2d8a0729286d!2z0JHRg9GC0YvRgNGB0LrQsNGPINGD0LsuLCAxNywg0JzQvtGB0LrQstCwLCAxMjcwMTU!5e0!3m2!1sru!2sru!4v1407247005704">посмотреть на карте</a>
					</p>

				</div>
				<div class="right_col text_light">
					<img src="images/dinner.png" title="Ужин Калугины" alt="Ужин Калугиных" />
					<h3>Ужин - 15:30 - 24:00</h3>
					<div class="clr"></div>
					<p>
						Ужин пройдёт в Ресторан "River terrace" :)<br/>
						<span>Адрес:</span> Московская обл., г. Химки, ул. Кудрявцева, д. 10,
51-й километр канала имени Москвы, в районе 73-х заград. ворот 
						
						<a class="popup fancybox.iframe" href="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2237.099363690336!2d37.462314899999996!3d55.895635999999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b538542390fe35%3A0xa086da00427d72ff!2z0YPQuy4g0JrRg9C00YDRj9Cy0YbQtdCy0LAsIDEwLCDQpdC40LzQutC4LCDQnNC-0YHQutC-0LLRgdC60LDRjyDQvtCx0LvQsNGB0YLRjCwgMTQxNDAx!5e0!3m2!1sru!2sru!4v1407248062032">посмотреть на карте</a> 
						 или <a href="http://river-terrace.ru/contacts" target="_blank">перейти на их сайт</a>
					
					</p>
				</div>
				<div class="clr"></div>
			</div>
		</div>
		<div class="block_before_footer"></div>
	</div>
	<div class="wrapp_footer">
		<div class="footer_line"></div>
		<div class="footer_logo">
			<div class="wrapp">
				<div class="phone">Засекречен :></div>
				<div class="mail">mail@happy-kalugins.ru</div>
				
				<div class="menu_footer">
					<a href="#history" >Наша история</a>
					<a href="#history" >Галлерея</a>
					<a href="#wedding_day" >ЗАГС</a>
					<a href="#wedding_day" >Ресторан</a>
				</div>
			<div class="clr"></div>
				
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/jquery.countdown.js"></script>
	<script src="js/script-for-end-body.js"></script>
</body>
</html>
